<?php

$routes = array(
    array('home','default','index'),


    array('users','user','index'),
    array('add-user','user','add'),
    array('edit-user','user','edit', array('id')),
    array('delete-user','user','delete', array('id')),

    array('salles','salle','index'),
    array('add-salle','salle','add'),
    array('edit-salle','salle','edit', array('id')),
    array('delete-salle','salle','delete', array('id')),

    array('creneaux','creneau','index'),
    array('add-creneau','creneau','add'),
    array('edit-creneau','creneau','edit', array('id')),
    array('delete-creneau','creneau','delete', array('id')),





);









