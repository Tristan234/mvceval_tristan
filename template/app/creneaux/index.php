<h1>Abonnés</h1>

<a class="add-item" href="<?= $view->path('add-creneau'); ?>"><h2>Add clients</h2></a>
<table>
    <tr>
        <th>Salle</th>
        <th>debut</th>
        <th>Nombre d'heures</th>
        <th>Actions</th>
    </tr>
    <?php foreach ($creneaux as $creneau) { ?>
        <tr>
            <td><?php echo $creneau->getIdSalle(); ?></td>
            <td><?php echo $creneau->getStartAt(); ?></td>
            <td><?php echo $creneau->getNbrehours(); ?> Heures</td>
            <td>
                <a onclick="return confirm('Voulez-vous supprimer ce creneau ?')" href="<?php echo $view->path('delete-creneau', array('id'=> $creneau->getId())) ?>">Supprimer</a>
            </td>
        </tr>
    <?php } ?>
</table>

<div class="paginator">
    <?php echo $paginator; ?>
</div>
