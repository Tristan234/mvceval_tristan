<h1>Salle</h1>

<a class="add-item" href="<?= $view->path('add-salle'); ?>"><h2>Add Salle</h2></a>
<table>
    <tr>
        <th>Nom de la salle</th>
        <th>Nombre de personne Maximum</th>
        <th>Actions</th>
    </tr>
    <?php foreach ($salles as $salle) { ?>
        <tr>
            <td><?php echo $salle->getTitle(); ?></td>
            <td><?php echo $salle->getMaxuser(); ?></td>
            <td>
                <a href="<?php echo $view->path('edit-salle', array('id'=> $salle->getId())) ?>">Editer</a> |
                <a onclick="return confirm('Voulez-vous supprimer cet salle ?')" href="<?php echo $view->path('delete-salle', array('id'=> $salle->getId())) ?>">Supprimer</a>
            </td>
        </tr>
    <?php } ?>
</table>

<div class="paginator">
    <?php echo $paginator; ?>
</div>
