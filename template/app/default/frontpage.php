<h1>Users</h1>
<table>
    <tr>
        <th>Nom</th>
        <th>Email</th>
        <th>Actions</th>
    </tr>
    <?php foreach ($users as $user) { ?>
        <tr>
            <td><?php echo $user->getNom(); ?></td>
            <td><?php echo $user->getEmail(); ?></td>
            <td>
                <a href="<?php echo $view->path('edit-user', array('id'=> $user->getId())) ?>">Editer</a> |
                <a onclick="return confirm('Voulez-vous supprimer ce produit ?')" href="<?php echo $view->path('delete-user', array('id'=> $user->getId())) ?>">Supprimer</a>
            </td>
        </tr>
    <?php } ?>
</table>

<div class="paginator">
    <?php echo $paginator; ?>
</div>
