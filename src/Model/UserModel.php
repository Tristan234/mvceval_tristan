<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class UserModel extends AbstractModel
{
    protected static $table = 'user';

    protected $id;
    protected $nom;
    protected $email;



    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (nom, email) VALUES (?,?)",
            array($post['nom'],$post['email'])
        );
    }

    public static function recipePaginate($itemsPerPage,$offset)
    {
        return App::getDatabase()->prepare(
            "SELECT * FROM " . self::getTable() . " LIMIT $itemsPerPage OFFSET $offset",
            [],
            get_called_class()
        );

    }

    public static function update($post,$id) {
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET nom = ?, email = ? WHERE id = ?",
            [$post['nom'],$post['email'],$id]
        );
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }


}