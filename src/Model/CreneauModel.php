<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class CreneauModel extends AbstractModel
{
    protected static $table = 'creneau';

    protected $id;
    protected $id_salle;
    protected $start_at	;
    protected $nbrehours;


    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (id_salle, start_at, nbrehours) VALUES (?,?,?)",
            array($post['id_salle'],$post['start_at'],$post['nbrehours'])
        );
    }

    public static function recipePaginate($itemsPerPage,$offset)
    {
        return App::getDatabase()->prepare(
            "SELECT * FROM " . self::getTable() . " LIMIT $itemsPerPage OFFSET $offset",
            [],
            get_called_class()
        );

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdSalle()
    {
        return $this->id_salle;
    }

    /**
     * @param mixed $id_salle
     */
    public function setIdSalle($id_salle): void
    {
        $this->id_salle = $id_salle;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->start_at;
    }

    /**
     * @param mixed $start_at
     */
    public function setStartAt($start_at): void
    {
        $this->start_at = $start_at;
    }

    /**
     * @return mixed
     */
    public function getNbrehours()
    {
        return $this->nbrehours;
    }

    /**
     * @param mixed $nbrehours
     */
    public function setNbrehours($nbrehours): void
    {
        $this->nbrehours = $nbrehours;
    }









}