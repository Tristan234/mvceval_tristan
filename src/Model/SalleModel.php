<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class SalleModel extends AbstractModel
{
    protected static $table = 'salle';

    protected $id;
    protected $title;
    protected $maxuser;



    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (title, maxuser) VALUES (?,?)",
            array($post['title'],$post['maxuser'])
        );
    }

    public static function recipePaginate($itemsPerPage,$offset)
    {
        return App::getDatabase()->prepare(
            "SELECT * FROM " . self::getTable() . " LIMIT $itemsPerPage OFFSET $offset",
            [],
            get_called_class()
        );

    }

    public static function update($post,$id) {
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET title = ?, maxuser = ? WHERE id = ?",
            [$post['title'],$post['maxuser'],$id]
        );
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function settitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getMaxuser()
    {
        return $this->maxuser;
    }

    /**
     * @param mixed $maxuser
     */
    public function setmaxuser($maxuser): void
    {
        $this->maxuser = $maxuser;
    }


}