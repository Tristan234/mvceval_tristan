<?php

namespace App\Controller;

use Core\Kernel\AbstractController;
use App\Controller\UserController;

/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {
        $users = UserController::all();
        $this->render('app.default.frontpage',array(
            'users' => $users,
        ));
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
