<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;
use http\user\Curl\User;
use JasonGrimes\Paginator;



class UserController extends DefaultController
{
    private $itemsPerPage = 5;
    public function index()
    {
        $currentPage = 1;
        if(!empty($_GET['paged']) && is_numeric($_GET['paged'])) {
            $currentPage = $_GET['paged'];
        }
        $totalItems = Usermodel::count();
        $urlPattern = '?paged=(:num)';
        $offset = ($currentPage - 1) * $this->itemsPerPage;
        $paginator = new Paginator($totalItems, $this->itemsPerPage, $currentPage, $urlPattern);
        $users = UserModel::recipePaginate($this->itemsPerPage,$offset);

        $this->render('app.users.index', array(
            'users' => $users,
            'paginator' => $paginator
        ));
    }

    public function add() {

        $errors = array();

        if(!empty($_POST['submitted'])){
            //faille xss
            $post = $this->cleanXss($_POST);
            //validation
            $v = new Validation();
            $errors = $this->validate($v,$post);

            if($v->IsValid($errors)) {
                UserModel::insert($post);
                $this->addFlash('success', 'Merci pour votre ajouts.');
                $this->redirect('users');
            }


        }
        $form = new Form($errors);

        $this->render('app.users.add', array(
            'form' => $form,
        ));
    }

    public function edit($id)
    {
        $user = $this->getuserByIdOr404($id);
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->IsValid($errors)) {
                UserModel::update($post,$id);
                $this->addFlash('success', 'Merci pour avoir édité le user.');
                $this->redirect('users');
            }
        }
        $form = new Form($errors);
        $this->render('app.users.edit', array(
            'form' => $form,
            'user' => $user,
        ));
    }

    private function getuserByIdOr404($id) {
        $user = UserModel::findById($id);
        if(empty($user)) {
            $this->Abort404();
        }
        return $user;
    }

    private function validate($v,$post)
    {
        $errors['nom'] = $v->textValid($post['nom'], 'nom', 2, 70);
        $errors['email'] = $v->emailValid($post['email']);

        return $errors;
    }

    public function delete($id) {
        $this->getuserByIdOr404($id);
        UserModel::delete($id);
        $this->addFlash('success', 'Merci pour avoir effacé la produits.');
        $this->redirect('users');

    }
}