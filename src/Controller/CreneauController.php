<?php

namespace App\Controller;

use App\Model\CreneauModel;;
use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;
use JasonGrimes\Paginator;



class CreneauController extends DefaultController
{
    private $itemsPerPage = 5;

    private $statusList;

    public function index()
    {
        $currentPage = 1;
        $salles = SalleModel::all();
        if(!empty($_GET['paged']) && is_numeric($_GET['paged'])) {
            $currentPage = $_GET['paged'];
        }
        $totalItems = CreneauModel::count();
        $urlPattern = '?paged=(:num)';
        $offset = ($currentPage - 1) * $this->itemsPerPage;
        $paginator = new Paginator($totalItems, $this->itemsPerPage, $currentPage, $urlPattern);
        $creneaux = CreneauModel::recipePaginate($this->itemsPerPage,$offset);

        $this->render('app.creneaux.index', array(
            'creneaux' => $creneaux,
            'paginator' => $paginator
        ));
    }

    public function add() {
        $errors = array();
        $salles = SalleModel::all();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors)) {
                CreneauModel::insert($post);
                $this->addFlash('success', 'Votre creneau a etais enregistré!');
                //$this->redirect('creneaux');
            }
        }
        $form = new Form($errors);
        $this->render('app.creneaux.add', array(
            'form' => $form,
            'salles' => $salles,
            'statusList' => $this->statusList
        ));
    }

    private function getcrenauByIdOr404($id) {
        $creneau = CreneauModel::findById($id);
        if(empty($creneau)) {
            $this->Abort404();
        }
        return $creneau;
    }

    private function validate($v,$post)
    {
        $errors = array();
//        if (!isset($post['heures']) || !is_numeric($post['heures']) || $post['heures'] < 10 || $post['heures'] > 0) {
//            $errors['heures'] = "L'heure maximal est de 8 heures";
//        }
        return $errors;
    }

    public function delete($id) {
        $this->getcrenauByIdOr404($id);
        CreneauModel::delete($id);
        $this->addFlash('success', 'Merci pour avoir effacé la produits.');
        $this->redirect('creneaux');

    }
}