<?php

namespace App\Controller;

use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;
use JasonGrimes\Paginator;



class SalleController extends DefaultController
{
    private $itemsPerPage = 5;
    public function index()
    {
        $currentPage = 1;
        if(!empty($_GET['paged']) && is_numeric($_GET['paged'])) {
            $currentPage = $_GET['paged'];
        }
        $totalItems = SalleModel::count();
        $urlPattern = '?paged=(:num)';
        $offset = ($currentPage - 1) * $this->itemsPerPage;
        $paginator = new Paginator($totalItems, $this->itemsPerPage, $currentPage, $urlPattern);
        $salles = SalleModel::recipePaginate($this->itemsPerPage,$offset);

        $this->render('app.salles.index', array(
            'salles' => $salles,
            'paginator' => $paginator
        ));
    }

    public function add() {

        $errors = array();

        if(!empty($_POST['submitted'])){
            //faille xss
            $post = $this->cleanXss($_POST);
            //validation
            $v = new Validation();
            $errors = $this->validate($v,$post);

            if($v->IsValid($errors)) {
                SalleModel::insert($post);
                $this->addFlash('success', 'Merci pour votre ajouts.');
                $this->redirect('salles');
            }


        }
        $form = new Form($errors);

        $this->render('app.salles.add', array(
            'form' => $form,
        ));
    }

    public function edit($id)
    {
        $salle = $this->getsalleByIdOr404($id);
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->IsValid($errors)) {
                SalleModel::update($post,$id);
                $this->addFlash('success', 'Merci pour avoir édité le salle.');
                $this->redirect('salles');
            }
        }
        $form = new Form($errors);
        $this->render('app.salles.edit', array(
            'form' => $form,
            'salle' => $salle,
        ));
    }

    private function getsalleByIdOr404($id) {
        $salle = SalleModel::findById($id);
        if(empty($salle)) {
            $this->Abort404();
        }
        return $salle;
    }

    private function validate($v,$post)
    {
        $errors['title'] = $v->textValid($post['title'], 'title', 2, 70);
        $errors['maxuser'] = $v->textValid($post['maxuser'], 'maxuser', 1, 4);

        return $errors;
    }

    public function delete($id) {
        $this->getsalleByIdOr404($id);
        SalleModel::delete($id);
        $this->addFlash('success', 'Merci pour avoir effacé la produits.');
        $this->redirect('salles');

    }
}